/* Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
   Author: Andrii Verbytskyi andrii.verbytskyi@mpp.mpg.de
*/
#ifndef ATLASHEPMC_HEAVYION_H
#define ATLASHEPMC_HEAVYION_H
#include "HepMC/HeavyIon.h"
#endif
